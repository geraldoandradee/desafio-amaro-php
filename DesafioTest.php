<?php

require_once __DIR__ . "/desafio.php";

class DesafioTest extends PHPUnit_Framework_TestCase
{
    private $json1;
    private $json2;

    public function setUp() {
        $this->json1 = readJson1();
        $this->json2 = readJson2();
    }

    public function testIfHeaderTransformWorks() {
        $this->assertArrayHasKey('medidas', $this->json1);
        $this->assertArraySubset($this->json2['header'], ["TAMANHO", "OMBROS", "BUSTO", "CINTURA", "COMPRIMENTO"]);
    }

    public function testIfRowsTransformWorks() {
        $result = buildRows($this->json1);

        $this->assertTrue(is_array($result), "Linhas nao podem ser null, precisa ser um array");

        $this->assertArrayHasKey("P", $result, "Chave P precisa existir");
        $this->assertArraySubset($result['P'], ["P", "60 cm", "100 cm", "100 cm", "95 cm"], 'Lista P esta diferente do esperado');

        $this->assertArrayHasKey("M", $result, "Chave M precisa existir");
        $this->assertArraySubset($result['M'], ["M", "62 cm", "104 cm", "104 cm", "96 cm"], 'Lista M esta diferente do esperado');

        $this->assertArrayHasKey("G", $result, "Chave G precisa existir");
        $this->assertArraySubset($result['G'], ["G", "63 cm", "110 cm", "110 cm", "97 cm"], 'Lista G esta diferente do esperado');

        $this->assertArrayHasKey("GG", $result, "Chave GG precisa existir");
        $this->assertArraySubset($result['GG'], ["GG", "64 cm", "114 cm", "114 cm", "98 cm"], 'Lista GG esta diferente do esperado');
    }

    public function testTransform() {
        $this->assertArraySubset(transform(), $this->json2 , 'O resultado final está diferente do esperado');
    }
}
