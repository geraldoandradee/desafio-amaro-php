<?php

function readJsonFile($filename) {
    return file_get_contents($filename);
}

function readJson1(){
    return json_decode(readJsonFile('json1.json'), true);
}

function readJson2(){
    return json_decode(readJsonFile('json2.json'), true);
}

function buildHeader($data) {
    $headers = [];
    array_push($headers, 'TAMANHO');

    if (array_key_exists('medidas', $data)) {
        foreach($data['medidas'] as $medida) {
            if (!in_array($medida['desc_medida'], $headers)) {
                array_push($headers, $medida['desc_medida']);
            }
        }
    }

    return $headers;
}


function buildRows($data) {
    $rows = [];

    if (array_key_exists('medidas', $data)) {
        foreach($data['medidas'] as $medida) {
            if (!array_key_exists($medida['tamanho'], $rows)) {
                $rows[$medida['tamanho']] = [$medida['tamanho'], $medida['valor'] . ' cm'];
            } else {
                array_push($rows[$medida['tamanho']], $medida['valor'] . ' cm');
            }
        }
    }

    return $rows;
}

function transform()
{
    $json1 = readJson1();
    echo "Old Json: " . print_r($json1, true);
    $data_transformed = [];
    $data_transformed["header"] = buildHeader($json1);
    $data_transformed["rows"] = buildRows($json1);

    return $data_transformed;
}

echo 'Transformed JSON: '. print_r(transform(), true);