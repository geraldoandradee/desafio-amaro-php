<?php

function readJsonFile($filename) {
    return file_get_contents($filename);
}

function readJson1(){
    return json_decode('{"medidas":[{"desc_medida":"OMBROS","tamanho":"G","valor":63},{"desc_medida":"OMBROS","tamanho":"GG","valor":64},{"desc_medida":"OMBROS","tamanho":"M","valor":62},{"desc_medida":"OMBROS","tamanho":"P","valor":60},{"desc_medida":"BUSTO","tamanho":"G","valor":110},{"desc_medida":"BUSTO","tamanho":"GG","valor":114},{"desc_medida":"BUSTO","tamanho":"M","valor":104},{"desc_medida":"BUSTO","tamanho":"P","valor":100},{"desc_medida":"CINTURA","tamanho":"G","valor":110},{"desc_medida":"CINTURA","tamanho":"GG","valor":114},{"desc_medida":"CINTURA","tamanho":"M","valor":104},{"desc_medida":"CINTURA","tamanho":"P","valor":100},{"desc_medida":"COMPRIMENTO","tamanho":"G","valor":97},{"desc_medida":"COMPRIMENTO","tamanho":"GG","valor":98},{"desc_medida":"COMPRIMENTO","tamanho":"M","valor":96},{"desc_medida":"COMPRIMENTO","tamanho":"P","valor":95}]}', true);
}

function readJson2(){
    return json_decode('{"header":["TAMANHO","OMBROS","BUSTO","CINTURA","COMPRIMENTO"],"rows":{"P":["P","60 cm","100 cm","100 cm","95 cm"],"M":["M","62 cm","104 cm","104 cm","96 cm"],"G":["G","63 cm","110 cm","110 cm","97 cm"],"GG":["GG","64 cm","114 cm","114 cm","98 cm"]}}', true);
}

function buildHeader($data) {
    $headers = [];
    array_push($headers, 'TAMANHO');

    if (array_key_exists('medidas', $data)) {
        foreach($data['medidas'] as $medida) {
            if (!in_array($medida['desc_medida'], $headers)) {
                array_push($headers, $medida['desc_medida']);
            }
        }
    }

    return $headers;
}


function buildRows($data) {
    $rows = [];

    if (array_key_exists('medidas', $data)) {
        foreach($data['medidas'] as $medida) {
            if (!array_key_exists($medida['tamanho'], $rows)) {
                $rows[$medida['tamanho']] = [$medida['tamanho'], $medida['valor'] . ' cm'];
            } else {
                array_push($rows[$medida['tamanho']], $medida['valor'] . ' cm');
            }
        }
    }

    return $rows;
}

function transform()
{
    $json1 = readJson1();
    echo "Old Json: " . print_r($json1, true);
    $data_transformed = [];
    $data_transformed["header"] = buildHeader($json1);
    $data_transformed["rows"] = buildRows($json1);

    return $data_transformed;
}

echo 'Transformed JSON: '. print_r(transform(), true);