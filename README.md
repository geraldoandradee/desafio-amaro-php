# Desafio Amaro

Desafio resolvido em php 5.5.14 disponível em https://hackpad.com/AMARO-Teste-de-programao-7X4s2pdqIUT. Resolução em https://repl.it/BW0M.


## Rode os testes

Antes de rodar os testes é preciso rodar o composer:
    
    php composer.phar install
    
    
Se achar prudente pode atualizar o composer:
    
    php composer.phar self-update
    

Para rodar os testes é simples (depois de rodar o composer):
    
    php ./vendor/bin/phpunit DesafioTest
    
    
## Rode o script

    php desafio.php